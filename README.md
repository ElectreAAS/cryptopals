# Cryptopals, but this time in Clojure

This repository contains a student's attempt at solving Matasano's cryptographic challenges in the Clojure language.

A complete list of the challenges can be found on [cryptopals website](https://cryptopals.com/).

An in-depth explanation of challenges 12 and 14 is located at [breaking_ecb](https://gitlab.com/ElectreAAS/cryptopals/-/blob/master/breaking_ecb.md).

## Author
Ambre Austen Suhamy

## Download
```
git clone https://gitlab.com/ElectreAAS/cryptopals.git
```
## Usage

This project was made using [Leiningen](https://leiningen.org), and I'd recommend using that tool for any Clojure project, including this one.

Each challenge is set up as a no-argument function in given set file.
The real implementation of the challenges are in separate files, grouped by theme.
To run a specific challenge, simply run
```
lein run challenge X
```
With X the absolute number of that challenge [1-64].

or
```
lein run set X challenge Y
```
with X and Y between 1 and 8.

To run all the challenges in a given set, run
```
lein run set X all
```
**Warning**: this may be very computationally demanding, if you have trouble running all of it at once, I'd recommend testing challenge after challenge.
The 14th challenge is the longest one and takes 3 whole minutes to run on my computer.

## License

Copyright © 2021

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
