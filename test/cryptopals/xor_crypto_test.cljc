(ns cryptopals.xor-crypto-test
  (:require [cryptopals.xor-crypto :as xc]
            [cryptopals.conversions :as cv]
            [clojure.test :refer [deftest is]]))

(deftest xor-test
  (is (= (xc/xor '(1 2 3) '(0))
         '(1 2 3)))
  (is (= (xc/xor '(1 2 3) '(4 5 6) '(7 8 9) '(10))
         '(2 15 6)))
  (is (= (xc/xor '(1 2 3) '(4 5 6) '(4 5 6))
         '(1 2 3)))
  (is (= (xc/xor '(0xff) '(0x00))
         '(0xff))))

(deftest single-xor-test
  (let [digits '(3 14 15 92 65 35 89 79 32 38 46 26 43 38 32 79 50 28 84 19 71)]
    (is (= (xc/single-xor digits 0)
           digits))
    (is (= (xc/single-xor digits 1)
          (map #(if (even? %) (inc %) (dec %)) digits)))))

(deftest break-single-xor-test
  (let [res (xc/break-single-xor '(27 55 55 51 49 54 63 120 21 27 127 43 120 52 49 51 61 120 57 120 40 55 45 54 60 120 55 62 120 58 57 59 55 54))]
    (is (= (cv/bytes->text (first res)) "Cooking MC's like a pound of bacon"))
    (is (< (second res) 256)))
  (is (< (second (xc/break-single-xor '(49 100 133 121 18 161 211 107))) 256)))

(deftest detect-single-xor-test
  (let [res (xc/detect-single-xor (slurp "resources/data1-4.txt"))]
    (is (> (count res) 0))
    (is (= (cv/bytes->text res) "Now that the party is jumping\n"))))

(deftest repeating-xor-test
  (is (= (xc/repeating-xor '(73 67 69)
           (cv/text->bytes "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"))
         (cv/hex->bytes "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f")))
  (is (= (xc/repeating-xor (cv/text->bytes "Otis")
           (cv/text->bytes "Vous savez, moi je ne crois pas qu'il y ait de bonne ou de mauvaise situation. Moi, si je devais résumer ma vie aujourd'hui avec vous, je dirais que c'est d'abord des rencontres. Des gens qui m'ont tendu la main, peut-être à un moment où je ne pouvais pas, où j'étais seul chez moi. Et c'est assez curieux de se dire que les hasards, les rencontres forgent une destinée... Parce que quand on a le goût de la chose, quand on a le goût de la chose bien faite, le beau geste, parfois on ne trouve pas l'interlocuteur en face je dirais, le miroir qui vous aide à avancer. Alors ça n'est pas mon cas, comme je disais là, puisque moi au contraire, j'ai pu : et je dis merci à la vie, je lui dis merci, je chante la vie, je danse la vie... je ne suis qu'amour ! Et finalement, quand beaucoup de gens aujourd'hui me disent \"Mais comment fais-tu pour avoir cette humanité ?\", et bien je leur reponds très simplement, je leur dis que c'est ce goût de l'amour ce goût donc qui m'a poussé aujourd'hui à entreprendre une construction mécanique, mais demain qui sait ? Peut-être simplement à me mettre au service de la communauté, à faire le don, le don de soi... "))
       (cv/hex->bytes "191b1c006f0708052a0e4553221b00532511491d2a540a01201d1a533f151a533e014e1a235410532e1d1d532b114911201a07166f1b1c532b11491e2e011f1226070c533c1d1d062e00001c215a493e201d45533c1d49192a540d16391500006f06aada3c0104163d5404126f0200166f151c1920011b17681c1c1a6f151f162c541f1c3a074553251149172606081a3c5418062a540a542a071d532b53081120060d532b111a533d110710201a1d012a0747530b111a53281107006f051c1a6f194e1c210049072a1a0d066f1808532215001d635419163a0044b0e5001b166fb7c9533a1a491e20190c1d3b5406b0f65403166f1a0c533f1b1c052e1d1a533f151a5f6f1baaca6f1e4eb0e600081a3c541a163a18491027111353221b005d6f311d532c530c003b5408003c1113532c011b1a2a0111532b1149002a540d1a3d1149023a11491f2a07491b2e0708012b07455323111a533d110710201a1d012a07491520060e1621004906211149172a071d1a21b7c016615a47531f151b102a5418062a5418062e1a0d53201a49126f180c53281baac83b540d166f1808532c1c06002a5849023a1507176f1b07532e5405166f1306b0f40049172a5405126f17011c3c11491126110753291500072a58491f2a540b162e0149142a071d16635419123d12061a3c54061d6f1a0c533b060606391149032e07491f681d07072a06051c2c011d163a06491621540f122c1149192a540d1a3d150000635405166f190001201d1b533e010053391b1c006f1500172a54aad36f151f1221170c016154281f20061a538cd3085321530c003b5419123c54041c21540a123c584910201904166f1e0c532b1d1a122607491f8cd445533f0100003e010c53221b00532e014910201a1d012e1d1b16635403542e1d49033a5453532a0049192a540d1a3c5404163d1700538cd4491f2e541f1a2a5849192a54050626540d1a3c5404163d17005f6f1e0c532c1c081d3b11491f2e541f1a2a5849192a540d1221070c53231549052611475d615403166f1a0c533c0100006f051c542e1906063d5448530a004915261a081f2a190c1d3b5849023a1507176f160c123a1706063f540d166f130c1d3c540806251b1c012b530106265404166f1000002a1a1d536d39081a3c540a1c22190c1d3b540f12260744073a54191c3a064912391b00016f170c073b11491b3a19081d2600aada6f4b4b5f6f111d532d1d0c1d6f1e0c5323111c016f060c03201a0d006f001bb0e70749002619191f2a190c1d3b5849192a5405163a064917260749023a11491068111a076f170c53281baac83b540d166f184e12221b1c016f170c53281baac83b540d1c211749023a1d491e6815490320011a008cdd49123a1e06063d104e1b3a1d49b0ef540c1d3b060c033d1107173d11490621114910201a1a073d010a07261b075322b7c0102e1a00023a114553221500006f100c1e2e1d07533e0100533c1500076f4b49232a011d5e8cde1d012a541a1a22040516221107076fb7c9532211491e2a001d012a5408066f070c01391d0a166f100c53231549102019040621151c078cdd45538cd449152e1d1b166f180c532b1b075f6f180c532b1b07532b114900201d475d6154"))))
