(ns cryptopals.utils-test
  (:require [cryptopals.utils :as utils]
            [clojure.test :refer [deftest is]]))

(deftest popcount-test
  (is (every? (partial = 1)
        (for [n (range 1 100)]
          (utils/popcount (.pow (biginteger 2) n)))))
  (is (every? (partial = 2)
        (for [n (range 1 100)]
          (utils/popcount (inc (.pow (biginteger 2) n))))))
  (is (every? identity
        (for [n (range 1 100)]
          (= n (utils/popcount (dec (.pow (biginteger 2) n))))))))

(deftest pad-bin-test
  (is (= (utils/pad-bin "1234567" 8)
         "01234567"))
  (is (= (utils/pad-bin "1234567" 7)
         "1234567"))
  (is (= (utils/pad-bin "123456789" 8)
         "0000000123456789"))
  (is (= (utils/pad-bin "1234567" 8 true)
         "12345670"))
  (is (= (utils/pad-bin "" 5)
         "")))

(deftest pad-bytes-test
  (is (= (utils/pad-bytes '(1 2 3 4 5 6 7) 8)
         (utils/pad-bytes '(1 2 3 4 5 6 7) 2)
         '(0 1 2 3 4 5 6 7)))
  (is (= (utils/pad-bytes '(1 2 3) 3)
         '(1 2 3)))
  (is (= (utils/pad-bytes '(1 2 3 4 5) 4)
         '(0 0 0 1 2 3 4 5)))
  (is (= (utils/pad-bytes '() 7)
         '())))

(deftest pad-pkcs7-test
  (is (= (utils/pad-pkcs7 (map byte "YELLOW SUBMARINE") 20)
         (map byte "YELLOW SUBMARINE\u0004\u0004\u0004\u0004")))
  (is (= (utils/pad-pkcs7 (map byte "YELLOW SUBMARINE") 16)
         (map byte "YELLOW SUBMARINE")))
  (is (= (utils/pad-pkcs7 (map byte "YELLOW SUBMARINE") 32)
         (map byte "YELLOW SUBMARINE\u0010\u0010\u0010\u0010\u0010\u0010\u0010\u0010\u0010\u0010\u0010\u0010\u0010\u0010\u0010\u0010"))))

(deftest unpad-pkcs7-test
  (is (= (map byte "YELLOW SUBMARINE")
         (utils/unpad-pkcs7 (map byte "YELLOW SUBMARINE"))))
  (is (= (utils/unpad-pkcs7
          (map byte "This text isn't long enough\u0005\u0005\u0005\u0005\u0005"))
         (map byte "This text isn't long enough"))))

(deftest validate-pkcs7-test
  (is (= (utils/validate-pkcs7 "ICE ICE BABY\u0004\u0004\u0004\u0004")
        "ICE ICE BABY"))
  (is (thrown? IllegalArgumentException
        (utils/validate-pkcs7 "ICE ICE BABY")))
  (is (thrown? IllegalArgumentException
        (utils/validate-pkcs7 "ICE ICE BABY\u0005\u0005\u0005\u0005")))
  (is (thrown? IllegalArgumentException
        (utils/validate-pkcs7 "ICE ICE BABY\u0001\u0002\u0003\u0004"))))

(deftest circular-shift-test
  (is (thrown? AssertionError (utils/circular-shift (map byte "1234") nil 2)))
  (is (thrown? AssertionError (utils/circular-shift (map byte "1234") "" 2)))
  (is (thrown? AssertionError (utils/circular-shift (map byte "1234") :l 2)))
  (is (= (utils/circular-shift '(36 00) :left 1)
         '(00 36)))
  (is (= (utils/circular-shift '(0x1a 0x2b 0x3c 0x4d 0x5e 0x6f) :right 0)
         '(0x1a 0x2b 0x3c 0x4d 0x5e 0x6f)))
  (is (= (utils/circular-shift '(0x1a 0x2b 0x3c 0x4d 0x5e 0x6f) :right 1)
         '(0x6f 0x1a 0x2b 0x3c 0x4d 0x5e)))
  (let [tests [["3600" (utils/circular-shift (map byte "3600") :left 1)]
               ["1a2b3c4d5e6f" (utils/circular-shift (map byte "1a2b3c4d5e6f") :right 0)]
               ["1a2b3c4d5e6f" (utils/circular-shift (map byte "1a2b3c4d5e6f") :right 1)]]]
    (is (every? #(= (count (first %)) (count (second %))) tests))))

(deftest weight-freqs-test
  (is (= (utils/weight-freqs "                   ")
         0))
  (is (= (utils/weight-freqs "coucou")
         88))
  (is (> (utils/weight-freqs "coucou\u0007")
         1000)))

(deftest bin-hamming-test
  (is (= (utils/bin-hamming 0xf0)
         4))
  (is (= (utils/bin-hamming 0x0f)
         4))
  (is (= (utils/bin-hamming 0xa0 0x73)
         5)))

(deftest hamming-dist-test
  (is (= (utils/hamming-dist "this is a test" "wokka wokka!!!" :text)
         37))
  (is (= (utils/hamming-dist "7468697320697320612074657374"
                            "776f6b6b6120776f6b6b61212121" :hex)
         37))
  (is (= (utils/hamming-dist "this" "wokk")
         8))
  (is (= (utils/hamming-dist '(1 2 3) '(1 2 4) :bytes)
         3)))

(deftest transpose-test
  (is (= (utils/transpose 4 (range 16))
         '((0 4 8 12)
           (1 5 9 13)
           (2 6 10 14)
           (3 7 11 15))))
  (is (= (utils/transpose 4 (range 14))
         '((0 4 8 12)
           (1 5 9 13)
           (2 6 10)
           (3 7 11))))
  (is (= (utils/transpose 4 (range 100))
         (list
          (for [i (range 25)] (* i 4)) ;; 0 4 8 12...
          (for [i (range 25)] (+ 1 (* i 4))) ;; 1 5 9 13...
          (for [i (range 25)] (+ 2 (* i 4))) ;; 2 6 10 14...
          (for [i (range 25)] (+ 3 (* i 4)))))) ;; 3 7 11 15...
  (is (= (utils/transpose 3 (range 8))
         '((0 3 6)
           (1 4 7)
           (2 5)))))

(deftest cookie-parse-test
  (is (= (utils/cookie-parse "foo=bar&baz=qux&a=b")
         {:foo "bar", :baz "qux", :a "b"}))
  (is (= (utils/cookie-parse "a=b&email=AuroraBorealis@_thistimeofyear")
         {:a "b", :email "AuroraBorealis@_thistimeofyear"}))
  (is (= (utils/cookie-parse "a=b&c=045")
         {:a "b" :c 45})))

(deftest cookie-format-test
  (is (= (utils/cookie-format {:email "bar", :uid 65 :role "b"})
         "email=bar&uid=065&role=b"))
  (is (= (utils/cookie-format {:email "bar", :uid 65 :role "b&cheat=enabled"})
         "email=bar&uid=065&role=b&cheat=enabled")))

(deftest create-profile-test
  (let [profile (utils/create-profile "foo@bar.com")]
    (is (= (dissoc profile :uid)
           {:email "foo@bar.com", :role "user"}))
    (is (int? (:uid profile)))
    (is (< -1 (:uid profile) 256)))
  (is (= (:email (utils/create-profile "foo@bar.com&role=admin"))
         "foo@bar.comroleadmin")))
