(ns cryptopals.block-cipher-crypto-test
  "A few of the functions to be tested in this file use some form of randomness,
  so all tests use some form of confidence interval."
  (:require [cryptopals.block-cipher-crypto :as bcc]
            [clojure.test :refer [deftest is]]))

(deftest predict-oracle-test
  (is (every? nil?
        (for [_ (range 0xff)]
          (bcc/predict-oracle bcc/encrypt-oracle)))))

(deftest random-key-test
  (let [try1 (bcc/random-key)
        try2 (bcc/random-key)
        try3 (bcc/random-key)
        try4 (bcc/random-key)
        try5 (bcc/random-key)]
    (is (= try1 try2 try3 try4 try5))))

(deftest random-prefix-test
  (let [try1 (bcc/random-prefix)
        try2 (bcc/random-prefix)
        try3 (bcc/random-prefix)
        try4 (bcc/random-prefix)
        try5 (bcc/random-prefix)]
    (is (= try1 try2 try3 try4 try5))))

(deftest round-trip-crypt-profile
  (is (= (dissoc (bcc/decrypt-profile (bcc/crypt-profile "hello")) :uid)
         {:email "hello", :role "user"})))

(deftest break-profile-test
  (is (= (dissoc (bcc/break-profile) :uid)
         {:email "Amber@is.gr8" :role "admin"}))
  (is (<= 0 (:uid (bcc/break-profile)) 255)))
