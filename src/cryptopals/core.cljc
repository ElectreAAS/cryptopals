(ns cryptopals.core
  (:require [cryptopals.challenges :as cc]))

(defn- print-err
  []
  (println "Please enter either 'chal[lenge]' or 'set' followed by a number."))

(defn- launch-args
  [word number]
  (if (re-matches #"[1-9]\d*" number)
    (let [n (read-string number)]
      (if (= word "set")
        (if (<= 1 n cc/nb-sets)
          (cc/set-full n)
          (println "I haven't implemented that set yet!"))
        (if (contains? #{"chal" "challenge"} word)
          (if (<= 1 n cc/nb-challenges)
            (cc/challenge n)
            (println "I haven't implemented that challenge yet!"))
          (print-err))))
    (print-err)))

(defn -main
  "Launch challenges according to user input."
  [& args]
  (if (= 2 (count args))
    (do
      (apply launch-args args)
      (shutdown-agents))
    (print-err)))
