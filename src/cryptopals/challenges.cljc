(ns cryptopals.challenges
  (:require [cryptopals.utils :as utils]
            [cryptopals.aes :as aes]
            [cryptopals.conversions :as cv]
            [cryptopals.xor-crypto :as xc]
            [cryptopals.block-cipher-crypto :as bcc]
            [clojure.string :refer [join split-lines]]))

(defn- challenge1
  []
  (cv/hex->64
    "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"))

(defn- challenge2
  []
  (cv/bytes->hex (xc/xor
                  (cv/hex->bytes "1c0111001f010100061a024b53535009181c")
                  (cv/hex->bytes "686974207468652062756c6c277320657965"))))

(defn- challenge3
  []
  (cv/bytes->text (first (xc/break-single-xor (cv/hex->bytes "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")))))

(defn- challenge4
  []
  (cv/bytes->text (xc/detect-single-xor (slurp "resources/data1-4.txt"))))

(defn- challenge5
  []
  (cv/bytes->hex
   (xc/repeating-xor (cv/text->bytes "ICE")
                     (cv/text->bytes "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"))))

(defn- challenge6
  []
  (let [[text key] (xc/break-repeating-xor (-> "resources/data1-6.txt"
                                               slurp
                                               split-lines
                                               join
                                               cv/base64->int
                                               cv/int->bytes))]
    (str "Key = \"" (cv/bytes->text key)
         "\" and Text =\n" (cv/bytes->text text))))

(defn- challenge7
  []
  (cv/bytes->text (aes/decrypt-ecb
                    (cv/text->bytes "YELLOW SUBMARINE")
                    (-> "resources/data1-7.txt"
                        slurp
                        split-lines
                        join
                        cv/base64->int
                        cv/int->bytes))))

(defn- challenge8
  []
  (aes/detect-ecb (slurp "resources/data1-8.txt")))

(defn- challenge9
  []
  (cv/bytes->text (utils/pad-pkcs7 (map byte "YELLOW SUBMARINE") 20)))

(defn- challenge10
  []
  (cv/bytes->text (aes/decrypt-cbc
                    (map byte "YELLOW SUBMARINE")
                    (-> "resources/data2-2.txt"
                        slurp
                        split-lines
                        join
                        cv/base64->int
                        cv/int->bytes))))

(defn- challenge11
  []
  (loop [i 0]
    (if (= i 100)
      "Successfully found the mode the box was using."
      (if-let [msg (bcc/predict-oracle bcc/encrypt-oracle)]
        msg
        (recur (inc i))))))


(defn- challenge12
  []
  (cv/bytes->text (bcc/break-ecb false)))

(defn- challenge13
  []
  (bcc/break-profile))

(defn- challenge14
  []
  (cv/bytes->text (bcc/break-ecb true)))

(defn- challenge15
  []
  (str "The function asked in this challenge is in utils:\n"
       "'validate-pkcs7', based on 'unpad-pkcs7'"))

(defn- challenge16
  []
  (bcc/decrypt-comments (bcc/break-comments)))

(def ^:private functions
  [challenge1 challenge2 challenge3 challenge4 challenge5 challenge6
   challenge7 challenge8 challenge9 challenge10 challenge11 challenge12
   challenge13 challenge14 challenge15 challenge16])

(def nb-challenges (count functions))

(def nb-sets (inc (* 8 (dec nb-challenges))))

(defn challenge
  [n]
  (println ((nth functions (dec n)))))

(defn set-full
  [n]
  (let [start-index (* 8 (dec n))
        funs (take 8 (drop start-index functions))
        handles (map future-call funs)]
    (doall
     (map-indexed
       (fn [i handle]
         (println (apply str (repeat 80 \-)))
         (println (str "Challenge " (+ start-index i 1) ":\n" @handle)))
       handles))))
