(ns cryptopals.utils
  (:require [clojure.string :as str :refer [index-of join]]))

(defn popcount
  "Count the number of 1-bits in given number."
  [n]
  (.bitCount (biginteger n)))

(defn pad-bin
  "Pad a binary number with zeroes until its size is a multiple of size.
  If little-endian? is given and true, pad on the right."
  ([number size] (pad-bin number size false))
  ([number size little-endian?]
   (let [n (mod (count number) size)]
     (if-not (zero? n)
       (let [to-add (join (repeat (- size n) \0))]
         (if little-endian?
           (str number to-add)
           (str to-add number)))
       number))))

(defn pad-bytes
  "Pad a byte sequence with zeroes until its size is a multiple of size.
  If little-endian? is given and true, pad on the right."
  ([s size] (pad-bytes s size false))
  ([s size little-endian?]
   (let [n (mod (count s) size)]
     (if-not (zero? n)
       (let [to-add (repeat (- size n) 0)]
         (if little-endian?
           (concat s to-add)
           (concat to-add s)))
       s))))

(defn pad-pkcs7
  "Pad given byte sequence 's' up to a multiple of 'size' bytes,
  using PKCS#7 scheme."
  [s size]
  (let [n (mod (count s) size)]
    (if-not (zero? n)
      (let [byte (- size n)]
        (lazy-cat s (repeat byte byte)))
      s)))

(defn unpad-pkcs7
  "Try to unpad 's' as if it was padded with PKCS#7 scheme."
  [s]
  (let [last (last s)]
    (if (and (< 0 last (count s))
             (every? #(= last %) (take-last last s)))
      (drop-last last s)
      s)))

(defn validate-pkcs7
  "Validate the padding of given string.
  Throws IllegalArgumentException in case of bad padding."
  [s]
  (let [byte-s (map #(mod % 256) (.getBytes s "UTF-8"))
        unpad-test (unpad-pkcs7 byte-s)]
    (if (not= byte-s unpad-test)
      (String. (byte-array (map unchecked-byte unpad-test)) "UTF-8")
      (throw (IllegalArgumentException. "Invalid padding")))))

(defn circular-shift
  "Circular shift byte sequence 's' in given direction, by 'shift' bytes."
  [s dir shift]
  {:pre [(contains? #{:left :right} dir)]}
  (if (= dir :left)
    (lazy-cat (drop shift s) (take shift s))
    (lazy-cat (take-last shift s) (drop-last shift s))))

(defn map-hex
  "Map on given hex string, byte after byte."
  [f hex]
  (map (comp f join) (partition 2 hex)))

(def letter-freqs-en
  " et,'.!?aoinshrdlcuI
mwfgypbvkjxqzETAONSRHLDCUMFPGWYBVKXJQZ\"-0123456789();:")

(defn weight-freqs
  "Weight the string 'msg' within the scope of given 'freqs'.
  No second argument means it's plain text english, so we use above string."
  ([msg] (weight-freqs msg letter-freqs-en))
  ([msg freqs]
   (reduce #(+ %1 (or (index-of freqs %2) 1000)) 0 msg)))

(defn bin-hamming
  "Computes the binary hamming distance of two numbers, or one byte and zero."
  ([n]
   (popcount n))
  ([m n]
   (popcount (.xor (biginteger m) (biginteger n)))))

(defn hamming-dist
  "Computes the binary hamming distance between two same-typed objects.
  With two args default type is text, optional third is for hex or bytes."
  ([l r] (hamming-dist l r :text))
  ([l r type]
   (let [[lb rb] (case type
                   :bytes [l r]
                   :text [(seq (.getBytes l "UTF-8")) (seq (.getBytes r "UTF-8"))]
                   :hex (map (fn [hex]
                               (map #(read-string (apply str "16r" %))
                                 (partition 2 (pad-bin hex 2))))
                          [l r]))]
     (if (not= (count lb) (count rb))
       (throw (ex-info "Invalid argument: these two should be the same size."
                {:left lb, :right rb :type type}))
       (reduce + 0 (map bin-hamming lb rb))))))

(defn transpose
  "Break down the byte sequence 's' as rows of 'size' bytes, and transpose it."
  [size s]
  {:pre [(pos? size)]}
  (->> s
       (partition size size (repeat ::transpose-fill))
       (apply map list)
       (map (partial remove #(= % ::transpose-fill)))))

(defmacro with-out-str-data-map
  [& body]
  `(let [s# (java.io.StringWriter.)]
     (binding [*out* s#]
       (let [r# ~@body]
         {:result r#
          :str    (str s#)}))))

(defn random-bytes
  "Generate a random byte sequence of size n, or 16 if no argument given."
  ([] (random-bytes 16))
  ([n] (repeatedly n #(rand-int 256))))

(defn cookie-parse
  "Parse a structured-cookie-like string (foo=bar&pi=3) into a Clojure map."
  [s]
  (let [matches (re-seq #"([a-zA-Z]\w*)=([\w@\.\-%]+)" s)]
    (into {} (map (fn [[_ l r]]
                    [(keyword l)
                     (if (every? #(<= (int \0) (int %) (int \9)) r)
                       (read-string (str "10r" r))
                       r)])
               matches))))

(defn cookie-format
  "Format a map into a structured-cookie-like string (foo=bar&pi=3)."
  [m]
  {:pre [(and (:email m) (:uid m) (:role m))]}
  (str "email=" (:email m)
       "&uid=" (format "%03d" (:uid m))
       "&role=" (:role m)))

(defn create-profile
  "Create a placeholder profile for a standard user, with given email adress."
  [email]
  {:email (str/replace email #"&|=" "")
   :uid (rand-int 256)
   :role "user"})

(defn may-print-char
  "If given char is printable, print it and flush.
  Otherwise add it to buff for next prints.
  All characters are assumed to be correct UTF-8."
  [c buff]
  (letfn [(print-flush [s] (do (print s) (flush) []))
          (pp [c] (print-flush (String. (byte-array
                                         (map unchecked-byte (conj buff c)))
                                 "UTF-8")))]
    (cond
      (<= 0 c 127) (pp c)

      (zero? (count buff)) (vector c)

      (<= 0xc2 (first buff) 0xdf)
      (pp c)

      (<= 0xe0 (first buff) 0xef)
      (if (= 1 (count buff))
        (conj buff c)
        (pp c))

      :else
      (if-not (= 3 (count buff))
        (conj buff c)
        (pp c)))))
