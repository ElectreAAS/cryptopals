(ns cryptopals.block-cipher-crypto
  "Everything in this file is kinda messy, I guess it will make more sense
  as building blocks in the future."
  (:require [cryptopals.aes :as aes]
            [cryptopals.xor-crypto :as xc]
            [cryptopals.conversions :as cv]
            [cryptopals.utils :refer [cookie-format cookie-parse random-bytes
                                      create-profile pad-bytes may-print-char]]
            [clojure.string :refer [join split-lines escape]]
            [clojure.data.priority-map :refer [priority-map-by]]))

(defn encrypt-oracle
  "Encrypt text using, at random, AES in CBC or ECB mode, with a random key.
  In CBC mode, it will also use a random initialization vector.
  It will also sneakily add useless bytes before and after the ciphertext."
  [text]
  (let [key (random-bytes)
        bytes-before (random-bytes (+ 5 (rand-int 6))) ; 5-10 random bytes
        bytes-after  (random-bytes (+ 5 (rand-int 6)))
        full-text (concat bytes-before text bytes-after)]
    (if (< 1 (rand 2))
      [(aes/encrypt-ecb key full-text) "ECB"]
      [(aes/encrypt-cbc key full-text) "CBC"])))

(defn predict-oracle
  "When given some black box function that might be using ECB or CBC mode,
  detect which one is in use."
  [box]
  ;; repeat produces a long sequence of identical bytes
  (let [[cipher used-mode] (box (repeat 256 (rand-int 256)))
        dupes (reduce + (map #(if (= %1 %2) 1 0)
                          cipher
                          (drop 16 cipher)))
        inferred-mode (if (> dupes 10) "ECB" "CBC")]
    (if-not (= used-mode inferred-mode)
      (str "Couldn't find the mode the box was using. (dupes: " dupes ")"))))

(def null-byte
  0)

(def random-key
  "This key will be randomly generated each time we run the program,
  but will stay constant during one execution."
  (memoize (fn [] (random-bytes))))

(def random-prefix
  "According to all litterature I could find, the intended way of doing
  the challenge #14 is with a constant (but random) prefix of constant
  (but random, again) size. I may do the other (nothing is constant)
  version in the future.
  This prefix is 0-63 random bytes, constant at runtime."
  (memoize (fn [] (random-bytes (rand-int 64)))))

(defn- help-break
  "Encrypt given text after appending a long string to it.
  We're trying to decode that very string.
  When second argument is given and true, add above random prefix to
  your text, to make decryption harder.
  This also changes the coded text to something else, to spice things up a bit."
  [attack-text with-prefix?]
  (let [target-text (-> (if-not with-prefix?
                          "resources/data2-4.txt"
                          "resources/data2-6.txt")
                        slurp
                        split-lines
                        join
                        cv/base64->int
                        cv/int->bytes)
        full-text (lazy-cat (when with-prefix? (random-prefix))
                            attack-text
                            target-text)]
      (aes/encrypt-ecb (random-key) full-text)))

(defn- find-sizes
  "Find the total size of the ciphertext+prefix and the block size the black box
  function is using. It would be 16 for everything related to AES."
  [box]
  (loop [n 1, init-size (count (box ()))]
    (let [test-size (count (box (repeat n null-byte)))]
      (if (= init-size test-size)
        (recur (inc n) init-size)
        [(- init-size (dec n)) (- test-size init-size)]))))

(defn- find-np-line
  "Find first non-prefix line:
  Given a black-box function that adds an unknown-sized-prefix to our text and
  the block size of that function, find the index of the first line of any
  'boxed' text that does not contain any byte from the prefix.
  This is done by sending 3 times the block size worth of 0-bytes, and finding
  duplicate lines in the result ciphertext.
  The index we are looking for will then be the index of the first twin line.
  Just to be sure, we also check that result on a similar set of cipher lines,
  but with 255-bytes sent instead, in case the prefix is full of 0-bytes."
  [box size]
  (let [cipher-lines (partition size (box (repeat (* 3 size) null-byte)))]
    (loop [i 0]
      (if (not= (nth cipher-lines i)
                (nth cipher-lines (inc i)))
        (recur (inc i))
        (let [verif-lines (partition size (box (repeat (* 3 size) (- 255 null-byte))))]
          (if (= (nth verif-lines i)
                 (nth verif-lines (inc i)))
            i
            (recur (inc i))))))))

(defn- find-prefix-size
  "Given a black-box function that adds an unknown-sized-prefix to our text,
  its block size, and the index calculated above, find the length of the last
  line of the prefix.
  This is done by first sending 2 times the block size worth of 0-bytes,
  then 2 times minus 2, then 2 times minus 3, etc. When we find a line different
  from the initial one, we have found the size we were looking for.
  The reason we start at 2 on the loop (instead of 0 or 1) is quite subtle:
  Since we look for differences and we want to return the last value without
  differences, at the end of the loop we return i-1. That excludes starting
  at 0. The only case where we would return 0 is when there is no prefix, which
  is already tested at the start of the function. We therefore don't need to
  check again, and we can start at 2 (minimum return value in the loop is 1)."
  [box size np-line]
  (if (pos? np-line)
    (let [null-line (nth (partition size
                           (box (repeat (* 2 size) null-byte)))
                        np-line)]
      (loop [i 2]
        (let [test-line (nth (partition size
                               (box (repeat (- (* 2 size) i) null-byte)))
                          np-line)]
          (if (= null-line test-line)
            (recur (inc i))
            (dec i)))))
    ;; np-line=0 means there is no prefix at all
    0))

(defn break-ecb
  "Break ecb with or without a prefix."
  [with-prefix?]
  (let [cipher-box #(help-break % with-prefix?)
        ;; Here temp-size is the size of prefix+text
        [temp-size size] (find-sizes cipher-box)
        first-np-line (find-np-line cipher-box size)
        prefix-size (find-prefix-size cipher-box size first-np-line)
        prefix-pad (mod (- size prefix-size) size)
        final-size (if (pos? first-np-line)
                     ;; We bring down final-size to only the text (no prefix)
                     (- temp-size (+ prefix-size (* size (dec first-np-line))))
                     temp-size)
        ;; A map of frequencies for each possible byte, to speed up search
        chars (apply priority-map-by > (interleave (range 256) (repeat 0)))
        ;; instead of calling cipher-box every time with the same arguments,
        ;; We store here the results of the empty-bytes calls and navigate them.
        standard-ciphers (for [i (range size)]
                           (partition size
                             (cipher-box (repeat (+ prefix-pad i) null-byte))))]
    ;; 's' is the number of 0-bytes to be added at every step
    ;; we start at size-1, then down to 0, and start again
    ;; 'char-buff' is just for pretty printing non-ascii characters
    (loop [res [], s (cycle (range (dec size) -1 -1)), chars chars, char-buff []]
      (let [current-line (nth (nth standard-ciphers (first s))
                           (+ first-np-line (quot (count res) size)))
            to-be-boxed (apply concat
                          (repeat prefix-pad null-byte)
                          (map #(pad-bytes (take-last size (conj res (first %)))
                                  size)
                            chars))
            test-lines (take 256 (drop first-np-line
                                   (partition size
                                     (cipher-box to-be-boxed))))
            sought-index (.indexOf test-lines current-line)
            sought-byte (first (nth (seq chars) sought-index))
            ;char-buff (may-print-char sought-byte char-buff)
            res (conj res sought-byte)]
        (if (not= final-size (count res))
          (recur res (rest s) (update chars sought-byte inc) char-buff)
          res)))))

(defn crypt-profile
  "Given an email, encrypt the corresponding profile using AES-ECB with
  a constant key."
  [email]
  (aes/encrypt-ecb (random-key)
    (cv/text->bytes (cookie-format (create-profile email)))))

(defn decrypt-profile
  "Parse an encrypted profile into a user map, using the same constant key."
  [profile]
  (cookie-parse (cv/bytes->text (aes/decrypt-ecb (random-key) profile))))

(defn break-profile
  "Create an admin profile using both above functions.
  A normal crypted profile has this structure:
  'email=_&uid=xxx&role=user' where 'email=' is 6 bytes long,
  and '&uid=xxx&role=user' is 18 bytes long.
  The encrypted string we want to create looks like this:
  email=__________
  __&uid=xxx&role=
  admin__padding__
  Since ECB will encrypt a line the same way wherever it is in the text,
  we first send 10 filler bytes followed by 'admin' and 11 padding bytes,
  creating this encrypted string:
  email=fillertext
  admin__padding__                 <-
  &uid=054&role=us                  |
  er______________                  |
  We then take only the second line |, which we'll put at the end of our made-up
  string after.
  To make that made-up string, we just need to take the first 2 lines of the
  result of encrypting a 12 bytes long email : |
  email=0123456789                            <-
  ab&uid=xxx&role=                            <-
  user____________"
  []
  (let [admin-line (take 16 (drop 16 ;; second line
                              (crypt-profile (str
                                              "fillertextadmin"
                                              (join (repeat 11 (char 11)))))))]
    (decrypt-profile (concat
                      (take 32 (crypt-profile "Amber@is.gr8")) ; first 2 lines
                      admin-line))))

(defn crypt-comments
  "Take an arbitrary input string, add something before and after it,
  and encrypt the result with AES-CBC with the classic random key."
  [text]
  (let [before "comment1=cooking%20MCs;userdata="
        escaped (escape text {\; "_" \= "_"})
        after ";comment2=%20like%20a%20pound%20of%20bacon"
        full-text (str before escaped after)]
    (aes/encrypt-cbc (random-key) (cv/text->bytes full-text))))

(defn decrypt-comments
  "Take something encrypted with AES-CBC, decrypt it, and look for the string
  ';admin=true;' in it. Return true if found, false otherwise."
  [coded]
  (let [decoded (cv/bytes->text (aes/decrypt-cbc (random-key) coded))]
    (str "Admin: " (some? (re-find #";admin=true;" decoded)))))

(defn break-comments
  "Break the previous functions to make the second one return true.
  Send 11 0-bytes to the first fun, then flip the bits of the result:
  By XOR-ing '%20MCs;user' with ';admin=true', we produce some gibberish in the
  whole block (so 'data=' will also be messed up). But the interesting part is
  that the block after that, containing our 0-bytes, will also be XOR-ed with
  ';admin=true'. Since they are 0s, it will produce that very string directly."
  []
  (let [encrypted (crypt-comments (join (repeat 11 \u0000)))
        flipped-line (xc/xor (take 16 (drop 16 encrypted))
                       (pad-bytes (cv/text->bytes ";admin=true") 16 true))]
    (concat (take 16 encrypted) flipped-line (drop 32 encrypted))))
