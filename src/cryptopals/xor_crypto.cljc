(ns cryptopals.xor-crypto
  (:require [cryptopals.conversions :as cv]
            [clojure.string :refer [split-lines]]
            [cryptopals.utils :as utils]))

(defn xor
  "Produce the byte sequence of the result of XORing all given byte sequences."
  [& args]
  (let [size (apply max (map count args))]
    (apply map bit-xor (map #(utils/pad-bytes % size) args))))

(defn single-xor
  "XOR all the bytes in byte seq 's' against single-byte 'key'."
  [s key]
  (xor s (repeat (count s) key)))

(defn break-single-xor
  "Break byte sequence 'input', XORing it against every possible byte.
  The input is assumed to have been single-key XORed."
  [input]
  (loop [key 0, min-score 0x7FFFFFFF, good-key -1, res ()]
    (if (< key 256)
      (let [decoded (single-xor input key)
            score (utils/weight-freqs (cv/bytes->text decoded))]
        (if (< score min-score)
          (recur (inc key) score key decoded)
          (recur (inc key) min-score good-key res)))
      [res, good-key])))

(defn detect-single-xor
  "Detect which line in given 'input' has been encrypted by single-key xor."
  [input]
  (loop [s (map cv/hex->bytes (split-lines input)), min-score 0x7FFFFFFF, msg ()]
    (if (seq s)
      (let [decoded-line (first (break-single-xor (first s)))
            score (utils/weight-freqs (cv/bytes->text decoded-line))]
        (if (< score min-score)
          (recur (rest s) score decoded-line)
          (recur (rest s) min-score msg)))
      msg)))

(defn repeating-xor
  "Sequentially apply each byte of the key against a byte of the text."
  [key text]
  (xor text (take (count text) (cycle key))))

(defn- find-key-size
  "Find a likely key size to decrypt a repeating-xor cipher.
  Helper function for the 'break-repeating-xor' sum function."
  [input]
  (loop [size 2, min-score 0x7FFFFFFF, good-size 0]
    (if (<= size 32)
      (let [pairs (partition (* 2 size) input)
            score (/ (reduce #(let [[l r] [(take size %2), (drop size %2)]]
                                (+ %1 (utils/hamming-dist l r :bytes)))
                             0 pairs)
                     size (count pairs))]
        (if (< score min-score)
          (recur (inc size) score size)
          (recur (inc size) min-score good-size)))
      good-size)))

(defn break-repeating-xor
  "Break repeating-XORed 'input'."
  [input]
  (let [size (find-key-size input)
        key (->> input
                 (utils/transpose size)
                 (map (comp second break-single-xor)))]
    [(repeating-xor key input) key]))
